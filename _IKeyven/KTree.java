package _IKeyven;

import java.util.ArrayList;
import java.util.List;

import org.ansj.domain.Term;

import _fileutil.Log;
import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.trees.Tree;

public class KTree {
	public static Tree getStanfordTree(List<Term> list) {
		List<TaggedWord> wordList = new ArrayList<TaggedWord>();
		for (Term tmp : list) {
			wordList.add(new TaggedWord(tmp.getName().trim(), null));
		}
		LexicalizedParser lp = _init.StanfordParser.getInstance().lp;
		if (lp == null) {
			Log.log("当前没有载入文法!");
			return null;
		}
		Tree parse = (Tree) lp.parse(wordList);
		return parse;
	}
}
