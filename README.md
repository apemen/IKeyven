```
// 在src\_IKeyven\MainTest.java中调用接口
System.out.println(str);
System.out.println(Function.compress(str, "src/_IKeyven/rules.txt",
	false));
System.out.println(Function.trunkhankey(str,
	"src/_IKeyven/deprules.txt", false));
System.out.println();
```
```
运行效果：
[
    美丽又善良的你被卑微的我深深地喜欢着……
    美丽又善良的你被卑微的我喜欢着……
    我喜欢你
]

[
    主干识别可以提高检索系统的智能
    提高智能
    主干识别提高智能
]

[
    夜晚的月亮是多么清澈。
    月亮是清澈。
    夜晚的月亮是多么清澈。
]

[
    它主要依靠市场价格的预测
    它主要依靠预测
    它依靠预测
]

[
    赌博的心态在投资中经常出现
    心态在投资中经常出现
    投资出现心态
]

[
    这个项目由我改善
    项目由我改善
    我改善项目
]

```